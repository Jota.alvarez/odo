"""
This is a python module used to modify the account_move module
"""

from odoo import models, fields

class AccountMove(models.Model):
    """
    This model is override to include, functionalities related to retentions module.
    """
    _name = "account.move"
    _inherit = "account.move"